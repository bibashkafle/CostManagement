package com.sc544.controllers;

import java.security.Principal;
import java.security.acl.Group;
import java.util.Date;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.sc544.module.ExpenseGroup;
import com.sc544.module.Role;
import com.sc544.module.User;
import com.sc544.service.IGroupService;
import com.sc544.service.IUserService;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Controller
public class HomeController {
	
	@Autowired IUserService userService;
	@Autowired IGroupService groupService;
	
	@GetMapping("/")
	public String hello(Model model, Principal principal){
		return getDashBoard(model,principal);
	}
	
	@GetMapping("/dashboard")
	public String getDashBoard(Model model, Principal principal){
		
		/*Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 String name = auth.getName(); *///get logged in username

		/* User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		 String name = user.getUserName();*/
		 
		String name = principal.getName();
		model.addAttribute("username", name);
		return "dashboard";
	}
	
	@GetMapping("/seeyou")
	public String seeYou(){
		return "logout";
	}
	
	@GetMapping("/addrole")
	public void configure(){
		groupService.addGroup(new ExpenseGroup("EA group"));
		groupService.addGroup(new ExpenseGroup("College group"));
		groupService.addGroup(new ExpenseGroup("Car group"));
		
		userService.addUser(new User("root", "root@123", new Date(),null));
		userService.addUser(new User("admin", "admin@123", new Date(),null));
		
		userService.assignGroup(1, 1);
		userService.assignGroup(2, 1);
		
		userService.addRole(new Role("ROLE_USER", null));
		userService.addRole(new Role("ROLE_ADMIN", null));
		
		userService.assignRole(1, 2);
		userService.assignRole(2, 1);
		
	}
}
