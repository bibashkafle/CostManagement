package com.sc544.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.sc544.module.*;
import com.sc544.service.IGroupService;
import com.sc544.service.IUserService;
import com.sc544.service.UserService;

@Controller
@SessionAttributes
public class UserController {

	@Autowired IUserService userService;
	@Autowired IGroupService groupService;
	
	@GetMapping(value = "/login")
	public String login(Model model) {
		return "login";
	}
	
	@GetMapping(value = "/userlist")
	public String showUserList(Model model) {
		model.addAttribute("users", userService.getAllUser());
		return "userList";
	}
	
	@GetMapping(value = "/getuser/{id}")
	public String showUserList(@PathVariable int id, Model model) {	
		model.addAttribute("users", userService.getUser(id));
		return "userManage";
	}
	
	@GetMapping(value = "/adduser")
	public String showUserForm(Model model) {	
		model.addAttribute("users",userService.getUser(0));
		return "userManage";
	}
	
	@PostMapping(value = "/saveuser")
	public String addUser(String id, String userName, String password, String confPassword) {
		User user = null;
		int data = id.isEmpty()?0:Integer.parseInt(id);
		if(data>0)
			user = new User(data,userName,password,new Date(),null);
		else
			user = new User(userName,password,new Date(),null);
		
		userService.addUser(user);
		return "redirect:/userlist";
	}
	
	@GetMapping(value="usergroups/{id}")
	public String showUsersGroup(@PathVariable int id, Model model){
		model.addAttribute("groups", groupService.getAllGroup());
		model.addAttribute("usersGroup",userService.getUsersGroup(id));
		model.addAttribute("userId",id);
		return "userGroupManage";
	}
	
	@PostMapping(value="/assigngroup")
	public String assignGroupToUser(String userid, String groupid){
		userid = userid.isEmpty()?"0":userid;
		groupid = groupid.isEmpty()?"0":groupid;
		userService.assignGroup(Integer.parseInt(userid), Integer.parseInt(groupid));
		return "redirect:/userGroupManage/"+userid;
	}
}
