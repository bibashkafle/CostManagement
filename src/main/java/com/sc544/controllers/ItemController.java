package com.sc544.controllers;

import java.security.Principal;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.sc544.module.ExpenseGroup;
import com.sc544.module.ItemBought;
import com.sc544.module.User;
import com.sc544.service.IGroupService;
import com.sc544.service.IItemBoughtService;
import com.sc544.service.IUserService;

@Controller
@SessionAttributes
public class ItemController {
	
	@Autowired IItemBoughtService itemBoughtService;
	@Autowired IGroupService iGroupService;
	@Autowired IUserService iUserService;
	
	@GetMapping(value = "/itemlist")
	public String showUserList(Model model) {
		model.addAttribute("items", itemBoughtService.getAllItemBought());
		return "itemList";
	}
	
	@GetMapping(value = "/additem")
	public String showItemForm(Model model) {
		model.addAttribute("groups",iGroupService.getAllGroup());		
		return "itemManage";
	}
	
	@GetMapping(value = "/getitem/{id}")
	public String showUserList(@PathVariable int id, Model model) {	
		model.addAttribute("items", itemBoughtService.getItemBought(id));
		return "itemManage";
	}
	

	@PostMapping(value = "/saveitem")
	public String addItemBought(String id, String description, String price, String note, String groupid, Principal principal) {
		id = id.isEmpty()?"0":id.replace(",", "").trim();
		groupid = groupid.isEmpty()?"0":groupid.replace(",", "").trim();
		ExpenseGroup group = iGroupService.getGroup(Integer.parseInt(groupid));
		
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		 String userName = principal.getName();
		
		User user = iUserService.getUserByName(userName);
		
		ItemBought item = new ItemBought(Integer.parseInt(id), description, new Date(), Float.parseFloat(price), null, note, group, user);
	    itemBoughtService.addItemBought(item);
		return "redirect:/itemList";
	}
}
