package com.sc544.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.sc544.module.ExpenseGroup;
import com.sc544.service.IGroupService;;

@Controller
@SessionAttributes
public class GroupController {
	
	@Autowired IGroupService groupService;

	@GetMapping(value = "/grouplist")
	public String showUserList(Model model) {	
		model.addAttribute("groups", groupService.getAllGroup());
		return "groupList";
	}
	
	@GetMapping(value = "/addgroup")
	public String showItemForm(Model model) {
		return "groupManage";
	}
	
	@GetMapping(value = "/getgroup/{id}")
	public String showUserList(@PathVariable int id, Model model) {	
		ExpenseGroup group = new ExpenseGroup(0, "");
		model.addAttribute("groups", group);
		return "groupManage";
	}
	
	
	@PostMapping(value = "/savegroup")
	public String addUser(String id, String groupName) {
		int dataId = id.isEmpty()?0:Integer.parseInt(id);
		if(dataId>0)
			groupService.addGroup(new ExpenseGroup(dataId, groupName));
		else
			groupService.addGroup(new ExpenseGroup(groupName));
		
		return "redirect:/groupList";
	}
}
