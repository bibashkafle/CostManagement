package com.sc544.config;


import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
    DataSource dataSource;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
              /*  .antMatchers("/css/**","/","/home","/login",
                		"/userlist", "/getuser/**", "/adduser", "/saveuser", "/assigngroup","/usergroups/**",
                		"/grouplist", "/getgroup/**", "/addgroup","/savegroup",
                		"/itemlist", "/getitem/**", "/additem","/saveitem"
                		).permitAll()*/
            	//.antMatchers("/", "/home","/addrole","/css/**").permitAll()
            	.antMatchers("/addrole","/css/**").permitAll()
                //.antMatchers("/products","/users","/addproduct","/product").hasAnyAuthority("ROLE_ADMIN")
                //.antMatchers("/order","/profile").hasAnyAuthority("USER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login").successForwardUrl("/dashboard")
                .passwordParameter("password")
                .usernameParameter("username")                
                .defaultSuccessUrl("/dashboard")
            	.permitAll()
            	.and()
            	.logout()
            	.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            	.logoutSuccessUrl("/seeyou")
                .permitAll();
         
        http.exceptionHandling().accessDeniedPage("/403");
        http.csrf().disable();
    }

  /*  @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    
        auth
            .inMemoryAuthentication()
                .withUser("user").password("pass").roles("ADMIN");
    }*/
    
	 @Autowired
	 public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
	   auth.jdbcAuthentication().dataSource(dataSource)
	   .usersByUsernameQuery("select user_name as username,password, enabled from user where user_name=?")
	   .authoritiesByUsernameQuery("SELECT u.user_name as username, r.name as role FROM user_roles ur JOIN user u on u.id = ur.users_id join role r on r.id = ur.roles_id where u.user_name =?"); 
	 }
}
