package com.sc544.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sc544.dao.IAccountDao;
import com.sc544.dao.IItemBoughtDao;
import com.sc544.module.Account;
import com.sc544.module.ItemBought;
import com.sc544.module.User;

@Service
@Transactional
public class ItemBoughtService implements IItemBoughtService {

	@Autowired IItemBoughtDao itemBoughtDao;
	
	@Autowired IAccountDao accountDao;
	
	@Override
	public void addItemBought(ItemBought item) {
		// TODO Auto-generated method stub
	
		itemBoughtDao.save(item);
		
		List<User> list = item.getGroup().getUsers();
		float amount = item.getPrice()/list.size();
		System.out.println("user size = "+list.size());
		System.out.println("amount = "+list.size());
		for (User user : list) {
			System.out.println("Usr = "+user.toString());
			Account ac = new Account(user, amount, new Date(), item, false);
			accountDao.save(ac);
		}
	}

	@Override
	public void deleteItemBought(int id) {
		// TODO Auto-generated method stub
		itemBoughtDao.delete(id);
	}

	@Override
	public ItemBought getItemBought(int id) {
		// TODO Auto-generated method stub
		return itemBoughtDao.findOne(id);
	}

	@Override
	public List<ItemBought> getAllItemBought() {
		// TODO Auto-generated method stub
		return itemBoughtDao.findAll();
	}
	
}
