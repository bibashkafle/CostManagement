/**
 * 
 */
package com.sc544.service;

import java.util.List;

import com.sc544.module.ExpenseGroup;

/**
 * @author bibash
 *
 */

public interface IGroupService {
	
	public void addGroup(ExpenseGroup group);
	
	public void deleteGroup(int id);
	
	public ExpenseGroup getGroup(int id);
	
	public List<ExpenseGroup> getAllGroup();
	
}
