package com.sc544.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sc544.dao.IGroupDao;
import com.sc544.dao.IRoleDao;
import com.sc544.dao.IUserDao;
import com.sc544.module.ExpenseGroup;
import com.sc544.module.Role;
import com.sc544.module.User;

@Service
@Transactional
public class UserService implements IUserService {
	
	@Autowired	IUserDao userDao;
	
	@Autowired	IGroupDao groupDao;
	
	@Autowired IRoleDao roleDao;
	
	@Override
	public void addUser(User user) {
		// TODO Auto-generated method stub
		userDao.save(user);
	}

	@Override
	public User getUser(int id) {
		// TODO Auto-generated method stub
		return userDao.findOne(id);
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		return userDao.findAll();
	}

	@Override
	public User doLogin(User user) {
		// TODO Auto-generated method stub
		//return userDao.findByName(user.getUserName());
		return null;
	}

	@Override
	public void assignGroup(int userId, int groupId) {
		// TODO Auto-generated method stub
		ExpenseGroup group = groupDao.getOne(groupId);
		User user = userDao.findOne(userId);
		group.addUser(user);
		groupDao.save(group);
	}

	@Override
	public List<ExpenseGroup> getUsersGroup(int userId) {
		// TODO Auto-generated method stub
		return userDao.findOne(userId).getGroup();
	}

	@Override
	public void addRole(Role role) {
		// TODO Auto-generated method stub
		roleDao.save(role);
	}

	@Override
	public void assignRole(int userId, int roleId) {
		// TODO Auto-generated method stub
		User user = userDao.findOne(userId);
		Role role = roleDao.findOne(roleId);
		user.addRoles(role);
		userDao.save(user);
	}

	@Override
	public User getUserByName(String userName) {
		// TODO Auto-generated method stub		
		List<User> list = userDao.findAll();
		for (User user : list) {
			if(user.getUserName().equals(userName))
				return user;
		}
		return new User();
	}
	
	
}
