/**
 * 
 */
package com.sc544.service;

import java.util.List;

import com.sc544.module.ExpenseGroup;
import com.sc544.module.Role;
import com.sc544.module.User;

/**
 * @author bibash
 *
 */
public interface IUserService {
	
	public void addUser(User user);
	
	public User getUser(int id);
	
	public List<User> getAllUser();
	
	public void assignGroup(int userId, int groupId);
	
	public List<ExpenseGroup> getUsersGroup(int userId);
	
	public User doLogin(User user);
	
	public void addRole(Role role);
	
	public void assignRole(int userId, int roleId);
	
	public User getUserByName(String userName);
}
