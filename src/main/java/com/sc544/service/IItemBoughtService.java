package com.sc544.service;

import java.util.List;

import com.sc544.module.ItemBought;

public interface IItemBoughtService {
	
	public void addItemBought(ItemBought item);
	
	public void deleteItemBought(int id);
	
	public ItemBought getItemBought(int id);
	
	public List<ItemBought> getAllItemBought();
	
}
