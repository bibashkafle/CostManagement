package com.sc544.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sc544.dao.IGroupDao;
import com.sc544.module.ExpenseGroup;

@Service
@Transactional
public class GroupService implements IGroupService {
	
	@Autowired
	private IGroupDao groupDao;
	
	
	@Override
	public void addGroup(ExpenseGroup group) {
		groupDao.save(group);
	}
	
	@Override
	public void deleteGroup(int id) {
		groupDao.delete(id);;
	}

	@Override
	public ExpenseGroup getGroup(int id) {
		return groupDao.findOne(id);
	}

	@Override
	public List<ExpenseGroup> getAllGroup() {
		return groupDao.findAll();
	}
}
