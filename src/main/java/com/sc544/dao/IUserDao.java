package com.sc544.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sc544.module.User;

@Transactional
public interface IUserDao extends JpaRepository<User, Integer> {
	
}
