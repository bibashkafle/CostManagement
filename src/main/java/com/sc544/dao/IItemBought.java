package com.sc544.dao;

import java.util.List;

import com.sc544.module.ItemBought;

public interface IItemBought {
	
	public void addPurchage(ItemBought item);
	
	public ItemBought getPurchaseById(int id);
	
	public List<ItemBought> getAllPurchase();
	
}
