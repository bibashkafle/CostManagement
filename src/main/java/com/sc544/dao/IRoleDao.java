package com.sc544.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sc544.module.ItemBought;
import com.sc544.module.Role;

@Transactional
public interface IRoleDao extends JpaRepository<Role, Integer>  {

}
