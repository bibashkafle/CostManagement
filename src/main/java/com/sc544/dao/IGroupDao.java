package com.sc544.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sc544.module.ExpenseGroup;

@Transactional
public interface IGroupDao extends JpaRepository<ExpenseGroup, Integer>  { //CustomGroupDao
	
}
