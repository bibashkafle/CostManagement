package com.sc544.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sc544.module.Account;

@Transactional
public interface IAccountDao extends JpaRepository<Account, Integer>  {

}
