package com.sc544.module;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class ExpenseGroup {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty
	@Column(unique=true)
	private String groupName;
	
	public ExpenseGroup(){
		
	}
	
	public ExpenseGroup(String groupName){
		this.groupName = groupName;
	}
	
	public ExpenseGroup(int id, String groupName){
		this.id = id;
		this.groupName = groupName;
	}
	
	@ManyToMany(cascade = CascadeType.PERSIST)
	private List<User> users = new ArrayList<>();
	
	@OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
	private List<ItemBought> itemBought = new  ArrayList<>();
	

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<ItemBought> getItemBought() {
		return itemBought;
	}

	public void setItemBought(List<ItemBought> itemBought) {
		this.itemBought = itemBought;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "ExpenseGroup [id=" + id + ", groupName=" + groupName + "]";
	}

	public void addUser(User user) {
		users.add(user);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
		result = prime * result + id;
		result = prime * result + ((itemBought == null) ? 0 : itemBought.hashCode());
		result = prime * result + ((users == null) ? 0 : users.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExpenseGroup other = (ExpenseGroup) obj;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		if (id != other.id)
			return false;
		if (itemBought == null) {
			if (other.itemBought != null)
				return false;
		} else if (!itemBought.equals(other.itemBought))
			return false;
		if (users == null) {
			if (other.users != null)
				return false;
		} else if (!users.equals(other.users))
			return false;
		return true;
	}
}
