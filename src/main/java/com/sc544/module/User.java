package com.sc544.module;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@NotEmpty
	@Column(unique=true)
	private String userName;

	public List<ExpenseGroup> getGroup() {
		return group;
	}

	public void setGroup(List<ExpenseGroup> group) {
		this.group = group;
	}
	
	@NotEmpty
	private String password;
	
	@NotNull
	private boolean enabled;
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "users")
	private List<ExpenseGroup> group;
	
	
	 @ManyToMany
	 private Collection<Role> roles = new ArrayList<>();
	 
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
	
	public void addRoles(Role role) {
		this.roles.add(role);
	}

	public User(){
		
	}
	
	public User(String userName, String password, Date createdDate, List<ExpenseGroup> group) {
		super();
		this.userName = userName;
		this.password = password;
		this.createdDate = createdDate;
		this.group = group;
		this.enabled = true;
	}
	
	public User(int id, String userName, String password, Date createdDate, List<ExpenseGroup> group) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.createdDate = createdDate;
		this.group = group;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", password=" + password + ", createdDate=" + createdDate
				+  "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + id;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (id != other.id)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	
}
