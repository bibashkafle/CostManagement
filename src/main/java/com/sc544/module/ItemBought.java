package com.sc544.module;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class ItemBought {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty
	private String description;
	
	@NotNull
	private Date date;
	
	@NotNull
	private float price;
	
	private String billUrl;
	
	private String note;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private ExpenseGroup group;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private User user;
	
	@OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
	private List<Account> account = new ArrayList<>();
	
	public ItemBought(){
		
	}
	
	public ItemBought(String description, Date date, float price, String billUrl, String note, ExpenseGroup group, User user) {
		super();
		this.description = description;
		this.date = date;
		this.price = price;
		this.billUrl = billUrl;
		this.note = note;
		this.group = group;
		this.user = user;
	}
	
	public ItemBought(int id, String description, Date date, float price, String billUrl, String note, ExpenseGroup group, User user) {
		super();
		this.id = id;
		this.description = description;
		this.date = date;
		this.price = price;
		this.billUrl = billUrl;
		this.note = note;
		this.group = group;
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getBillUrl() {
		return billUrl;
	}

	public void setBillUrl(String billUrl) {
		this.billUrl = billUrl;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public ExpenseGroup getGroup() {
		return group;
	}

	public void setGroup(ExpenseGroup group) {
		this.group = group;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "ItemBought [id=" + id + ", description=" + description + ", date=" + date + ", price=" + price
				+ ", billUrl=" + billUrl + ", note=" + note+"]";
	}
	
	
}
