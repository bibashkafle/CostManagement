package com.sc544.module;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Account {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@ManyToOne
	@NotNull
	private User user;
	
	@NotNull
	private float amount;
	
	@NotNull
	private Date date;
	
	@ManyToOne
	@NotNull
	private ItemBought item;
	
	@NotNull
	private boolean isSettle;

	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public float getAmount() {
		return amount;
	}



	public void setAmount(float amount) {
		this.amount = amount;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}



	public ItemBought getItem() {
		return item;
	}



	public void setItem(ItemBought item) {
		this.item = item;
	}



	public boolean isSettle() {
		return isSettle;
	}



	public void setSettle(boolean isSettle) {
		this.isSettle = isSettle;
	}



	public int getId() {
		return id;
	}



	public Account(User user, float amount, Date date, ItemBought item, boolean isSsettle) {
		super();
		this.user = user;
		this.amount = amount;
		this.date = date;
		this.item = item;
		this.isSettle = isSsettle;
	}
}
