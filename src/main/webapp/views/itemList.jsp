<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all" href="css/style.css" />
<title>Item List</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<h3>Item List</h3>
		<ul class="nav nav-tabs">
			<li class="active"><a href="itemlist">Item List</a></li>
			<li><a href="additem">Add/Update Group</a></li>
		</ul>

		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Item Name</th>
						<th>Price</th>
						<th>Date</th>
						<th>Group</th>
						<th>View</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="item" items="${items}">
						<tr>
							<td>${item.id}</td>
							<td>${item.description}</td>
							<td>${item.price}</td>
							<td>${item.date}</td>
							<td>${item.group.groupName}</td>
							<td><a href="getgroup/${item.id}">Edit</a></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>

	</div>
</body>
</html>