<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all" href="css/style.css" />
<title>Group List</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<h3>Add/Update User</h3>
		<ul class="nav nav-tabs">
			<li  class="active"><a href="grouplist">Group List</a></li>
			<li><a href="addgroup">Add/Update Group</a></li>
		</ul>
		
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Group Name</th>
						<th>Edit</th>
						<!-- <th>Group</th>		 -->				
					</tr>
				</thead>
				<tbody>
					
					<c:forEach var="group" items ="${groups}">
						<tr>
						<td>${group.id}</td>
						<td>${group.groupName}</td>
						 <td><a href="getgroup/${group.id}">Edit</a></td>	
						</tr>
					</c:forEach>
					
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>