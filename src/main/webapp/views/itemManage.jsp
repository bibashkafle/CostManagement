<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all" href="css/style.css" />
<title>Add New Item</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		
		<h3>Add/Update Item</h3>
		<ul class="nav nav-tabs">
			<li><a href="itemlist">Item List</a></li>
			<li  class="active"><a href="additem">Add/Update Item</a></li>
		</ul>
		
		<form action="saveitem" method="post">
		<div class="form-group">
				<label>Group</label>
				<select name="groupid" class="form-control">
					<c:forEach var="group" items="${groups}">
					 	<option value="${group.id}">${group.groupName}</option>
					</c:forEach>
				</select>
				
			</div>
			
			<div class="form-group">
				<label>Item</label> <input class="form-control" type="text"
					name="description">
			</div>
			
			<div class="form-group">
				<label>Price</label> <input class="form-control" type="text"
					name="price">
			</div>
			
			<div class="form-group">
				<label>Remarks</label> 
				<textarea class="form-control" type="text"
					name="note"> </textarea>
			</div>
			
			 <input type="hidden" name="groupid" value="">
			 <input type="hidden" name="id" value="">
			 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.tocken}">
			<button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>
</body>
</html>