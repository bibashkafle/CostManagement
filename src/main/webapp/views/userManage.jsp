<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all" href="css/style.css" />
<title>Add New User</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		
		<h3>Add/Update User</h3>
		<ul class="nav nav-tabs">
			<li><a href="userlist">User List</a></li>
			<li  class="active"><a href="adduser">Add New User</a></li>
		</ul>
		
		<form action="saveuser" method="post">
			<div class="form-group">
				<label>User name</label> <input class="form-control" type="text"
					name="userName">
			</div>
			<div class="from-group">
				<label>Password</label> <input class="form-control" type="password"
					name="password">
			</div>
			<div class="from-group">
				<label>Confirm Password</label> <input class="form-control" type="password"
					name="confirmPassword">
			</div>
			<input type="hidden" name="id" value="">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.tocken}">
			<button type="submit" class="btn btn-default">Submit</button>
		</form>
	</div>
</body>
</html>