<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Cost Management</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      <li><a href="http://localhost:8080/itemlist">Item</a></li>
      <sec:authorize access="hasRole('ROLE_ADMIN')">
       <li><a href="http://localhost:8080/grouplist">Group</a></li>
       <li><a href="http://localhost:8080/userlist">User</a></li>
       </sec:authorize>
       <li><a href="http://localhost:8080/userlist">Owe</a></li>
       <li><a href="http://localhost:8080/logout">logout</a></li>
    </ul>
  </div>
</nav>