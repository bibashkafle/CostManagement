<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all"
	href="http://localhost:8080/css/style.css" />
<title>Login</title>
<style type="text/css">
body {
	background: #eee !important;
}

.wrapper {
	margin-top: 80px;
	margin-bottom: 80px;
}

.form-signin {
	max-width: 380px;
	padding: 15px 35px 45px;
	margin: 0 auto;
	background-color: #fff;
	border: 1px solid rgba(0, 0, 0, 0.1);
	.
	form-signin-heading
	,
	.checkbox
	{
	margin-bottom
	:
	30px;
}

.checkbox {
	font-weight: normal;
}

.form-control {
	position: relative;
	font-size: 16px;
	height: auto;
	padding: 10px;
	@
	include
	box-sizing(border-box);
	&:
	focus
	{
	z-index
	:
	2;
}

}
input[type="text"] {
	margin-bottom: -1px;
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
}

input[type="password"] {
	margin-bottom: 20px;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}
}
</style>
</head>
<body>

	<div class="container">
<%-- 	<div th:if="${param.error}">Invalid username and password.</div>
	<div th:if="${param.logout}">You have been logged out.</div> --%>
		<div class="wrapper">
			<form class="form-signin" th:action="@{/login}" method="post">
				<h2 class="form-signin-heading">Please login</h2>
				<input type="text" class="form-control" name="username"
					placeholder="User Name" required="required" autofocus="" /> <input
					type="password" class="form-control" name="password"
					placeholder="Password" required="required" />

				<button class="btn btn-lg btn-default btn-block" type="submit">Login</button>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.tocken}">
			</form>
		</div>
	</div>
</body>
</html>