<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all" href="css/style.css" />
<title>User List</title>
</head>
<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<h3>Add/Update User</h3>
		<ul class="nav nav-tabs">
			<li class="active"><a href="userlist">User List</a></li>
			<li><a href="adduser">Add New User</a></li>
		</ul>
			
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>User Name</th>
						<th>Edit</th>
						<th>Assign group</th>
						<!-- <th>Group</th>		 -->				
					</tr>
				</thead>
				<tbody>
					
					<c:forEach var="user" items ="${users}">
						<tr>
						<td>${user.id}</td>
						<td>${user.userName}</td>
						 <td><a href="getuser/${user.id}">Edit</a></td>	
						 <td><a href="usergroups/${user.id}">Group</a></td>	
						</tr>
					</c:forEach>
					
				</tbody>
			</table>
		</div>

	</div>


</body>
</html>