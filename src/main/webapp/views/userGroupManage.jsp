<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all" href="http://localhost:8080/css/style.css" />
<title>Assign Group</title>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<h3>Assign group to user</h3>
		
			<form action="http://localhost:8080/assigngroup" method="post">
			<div class="form-group">
					<label>Group</label>
					<select name="groupid" class="form-control">
						<c:forEach var="group" items="${groups}">
						 	<option value="${group.id}">${group.groupName}</option>
						</c:forEach>
					</select>					
				</div>
				<input type="hidden" name="userid" value="${userId}">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.tocken}">
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
			
			<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Group Name</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="group" items="${usersGroup}">
						<tr>
							<td>${group.id}</td>
							<td>${group.groupName}</td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
		
	</div>
</body>
</html>